import configparser
import os
import json

class recon_directory:
    
    def __init__(self, config_info, url):
        self.config_info = config_info
        self.url = url
        pass


    def fuzzing_url_using_ffuf(self):
        #ffuf -u https://blog.webico.vn/FUZZ -w /home/quockhai/Desktop/Project/recon-tool/SecLists/Discovery/Web-Content/directory-list-2.3-small.txt -ic -o abc.txt -mc 200 -recursion
        cmd = "ffuf -u " + self.url + "FUZZ" + " -w " + self.config_info['list_payload'] + " -o " + self.config_info["file_output_ffuf"] + " -mc 200 -recursion -ic"
        os.system(cmd)
        self.readfile_output_ffuf_and_handling()


    def readfile_output_ffuf_and_handling(self):
        
        #Đọc file output của ffuf (file output của ffuf sẽ ở dạng json)
        file_output_ffuf = open(self.config_info["file_output_ffuf"], "r")

        file_output_ffuf_readlines = file_output_ffuf.readlines()

        #Chuyển Data về định dạng json để xử lý
        file_output_ffuf_readlines_json = json.loads(file_output_ffuf_readlines[0])

        file_output_ffuf.close()

        #Đọc file report và lưu thông tin về các url có status 200

        file_report_url_200 = open(self.config_info["file_report_url_200"], "w")

        for i in file_output_ffuf_readlines_json['results']:
            file_report_url_200.writelines(i['url'] + '\n')
            


    
