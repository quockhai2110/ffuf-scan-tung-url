import os
import configparser
import json
from recon_directory import recon_directory
import sys


if __name__ == '__main__':
    #Đọc file config
    config_obj = configparser.ConfigParser()
    config_obj.read("config.ini")

    config_info = config_obj["ffuf"]

    recon = recon_directory( config_info, sys.argv[1])
    #Run ffuf
    recon.fuzzing_url_using_ffuf()

    